﻿using System;
using TankCommon;
using TankCommon.Enum;
using TankCommon.Objects;

namespace TankClient
{
    public class MyBot : IClientBot
    {
        /// <param name="rectangle"> 
        /// Хранит местоположение танка.
        /// </param>
        protected Rectangle rectangle;

        /// <param name="_map"> 
        /// Хранит данные о карте.
        /// </param>
        protected Map _map;
        protected readonly Random _random = new Random();

        /// <param name="isMoving"> 
        /// Перключатель движения.
        /// </param>
        protected bool isMoving = true;



        public ServerResponse Client(int msgCount, ServerRequest request)
        {
            /// <param name="myTank"> 
            /// Хранит данные о танке, полученном с сервера.
            /// </param>
            var myTank = request.Tank;

            /// <summary>
            /// Проверка на существование танка.
            /// </summary>
            if (myTank == null)
            {
                return new ServerResponse { ClientCommand = ClientCommandType.None };
            }


            _map = SetClassMap(_map, request);

            /// <summary>
            /// Запись интерактивных объектов.
            /// </summary>
            _map.InteractObjects = request.Map.InteractObjects;
            /// <summary>
            /// Проверка на существование карты.
            /// </summary>
            if (_map == null)
            {
                return new ServerResponse { ClientCommand = ClientCommandType.UpdateMap };
            }


            if (isMoving)
            {       
                return new ServerResponse { ClientCommand = ClientCommandType.Fire };
            }
            else
            {
                if (!checkObstruction(_map, myTank, myTank.Direction))
                {
                    return new ServerResponse { ClientCommand = ClientCommandType.Go };
                }
                else
                {
                    return new ServerResponse { ClientCommand = SwitchDirection() };
                }
            }

        }


        /// <summary>
        /// Запись карты в переменную.
        /// </summary>
        private Map SetClassMap(Map map, ServerRequest request)
        {
            if (request.Map.Cells != null)
            {
                _map = request.Map;
            }
            return _map;
        }


        /// <summary>
        /// Проверка на существование препятствия перед танком.
        /// </summary>
        /// <param name="direction"> 
        /// Направление движения танка.
        /// </param>
        private bool checkObstruction(Map map, TankObject myTank, DirectionType direction)
        {
            var firstCanGo = true;
            var secondCanGo = true;

            /// <summary>
            /// Ближний угол танка.
            /// </summary>
            if (direction == DirectionType.Left &&
                (map.Cells[GetY(myTank), GetX(myTank) - (Constants.CellWidth - 2)] != CellMapType.Wall &&
                map.Cells[GetY(myTank), GetX(myTank) - (Constants.CellWidth - 2)] != CellMapType.DestructiveWall &&
                map.Cells[GetY(myTank), GetX(myTank) - (Constants.CellWidth - 2)] != CellMapType.Water))  
            {
                firstCanGo = false;
            }
                
            if (direction == DirectionType.Up &&
                (map.Cells[GetY(myTank) - (Constants.CellWidth - 2), GetX(myTank)] != CellMapType.Wall &&
                map.Cells[GetY(myTank) - (Constants.CellWidth - 2), GetX(myTank)] != CellMapType.DestructiveWall &&
                map.Cells[GetY(myTank) - (Constants.CellWidth - 2), GetX(myTank)] != CellMapType.Water))
            {
                firstCanGo = false;
            }
            if (direction == DirectionType.Down &&
                map.Cells[GetY(myTank) + ((Constants.CellWidth * 2) - 2), GetX(myTank)] != CellMapType.Wall &&
                map.Cells[GetY(myTank) + ((Constants.CellWidth * 2) - 2), GetX(myTank)] != CellMapType.DestructiveWall &&
                map.Cells[GetY(myTank) + ((Constants.CellWidth * 2) - 2), GetX(myTank)] != CellMapType.Water)
            {
                firstCanGo = false;
            }
            if (direction == DirectionType.Right &&
                (map.Cells[GetY(myTank), GetX(myTank) + (Constants.CellWidth * 2 - 2)] != CellMapType.Wall &&
                map.Cells[GetY(myTank), GetX(myTank) + (Constants.CellWidth * 2 - 2)] != CellMapType.DestructiveWall &&
                map.Cells[GetY(myTank), GetX(myTank) + (Constants.CellHeight * 2 - 2)] != CellMapType.Water))
            {
                firstCanGo = false;
            }


            /// <summary>
            /// Дальний угол танка.
            /// </summary>
            if (direction == DirectionType.Left &&
                (map.Cells[GetY(myTank) + (Constants.CellHeight - 1), GetX(myTank) - (Constants.CellWidth - 2)] != CellMapType.Wall &&
                map.Cells[GetY(myTank) + (Constants.CellHeight - 1), GetX(myTank) - (Constants.CellWidth - 2)] != CellMapType.DestructiveWall &&
                map.Cells[GetY(myTank) + (Constants.CellHeight - 1), GetX(myTank) - (Constants.CellWidth - 2)] != CellMapType.Water))
            {
                secondCanGo = false;
            }
            if (direction == DirectionType.Up &&
                (map.Cells[GetY(myTank) - (Constants.CellWidth - 2), GetX(myTank) + (Constants.CellHeight - 1)] != CellMapType.Wall &&
                map.Cells[GetY(myTank) - (Constants.CellWidth - 2), GetX(myTank) + (Constants.CellHeight - 1)] != CellMapType.DestructiveWall &&
                map.Cells[GetY(myTank) - (Constants.CellWidth - 2), GetX(myTank) + (Constants.CellHeight - 1)] != CellMapType.Water))
            {
                secondCanGo = false;
            }
            if (direction == DirectionType.Down &&
                (map.Cells[GetY(myTank) + (Constants.CellWidth * 2 - 2), GetX(myTank) + (Constants.CellHeight - 1)] != CellMapType.Wall &&
                map.Cells[GetY(myTank) + (Constants.CellWidth * 2 - 2), GetX(myTank) + (Constants.CellHeight - 1)] != CellMapType.DestructiveWall &&
                map.Cells[GetY(myTank) + (Constants.CellWidth * 2 - 2), GetX(myTank) + (Constants.CellHeight - 1)] != CellMapType.Water))
            {
                secondCanGo = false;
            }
            if (direction == DirectionType.Right &&
                (map.Cells[GetY(myTank) + (Constants.CellHeight - 1), GetX(myTank) + (Constants.CellWidth * 2 - 2)] != CellMapType.Wall &&
                map.Cells[GetY(myTank) + (Constants.CellHeight - 1), GetX(myTank) + (Constants.CellWidth * 2 - 2)] != CellMapType.DestructiveWall &&
                map.Cells[GetY(myTank) + (Constants.CellWidth - 1), GetX(myTank) + (Constants.CellHeight * 2 - 2)] != CellMapType.Water))
            {
                secondCanGo = false;
            }
            return (firstCanGo && secondCanGo);
        }

        /// <summary>
        /// Отступ от левой границы. Т.е. X - координата танка.
        /// </summary>
        private static int GetX(BaseInteractObject interObj)
        {
            return interObj.Rectangle.LeftCorner.LeftInt;
        }

        /// <summary>
        /// Отступ от верхней границы. Т.е. Y - координата танка.
        /// </summary>
        private static int GetY(BaseInteractObject interObj)
        {
            return interObj.Rectangle.LeftCorner.TopInt;
        }


        /// <summary>
        ///  Рандомная смена направления.
        /// </summary>
        protected ClientCommandType SwitchDirection()
        {
            var rndNum = _random.Next(4);
            switch (rndNum)
            {
                case 0:
                    return ClientCommandType.TurnLeft;
                case 1:
                    return ClientCommandType.TurnRight;
                case 2:
                    return ClientCommandType.TurnUp;
                default:
                    return ClientCommandType.TurnDown;
            }
        }
    }
}